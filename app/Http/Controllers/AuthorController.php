<?php

namespace App\Http\Controllers;

use App\Services\AuthorService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthorController extends Controller
{
    use ApiResponser;

    /**
     * @var AuthorService
     */
    public $authorService;

    /**
     * AuthorController constructor.
     * @param AuthorService $authorService
     */
    public function __construct(AuthorService $authorService)
    {
        $this->authorService = $authorService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $authors = $this->authorService->obtainAuthors();

        return $this->successResponse($authors);
    }


    public function store(Request $request)
    {
        $author = $this->authorService->createAuthor($request->all(), Response::HTTP_CREATED);

        return $this->successResponse($author);
    }

    public function show($author)
    {
        $author = $this->authorService->obtainAuthor($author);

        return $this->successResponse($author);
    }

    public function update(Request $request, $author)
    {
        $author = $this->authorService->editAuthor($request->all(), $author);

        return $this->successResponse($author);
    }

    public function destroy($author)
    {
        $author = $this->authorService->deleteAuthor($author);

        return $this->successResponse($author);
    }

}
