<?php

namespace App\Http\Controllers;

use App\Services\AuthorService;
use App\Services\BookService;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookController extends Controller
{
    use ApiResponser;

    /**
     * @var BookService
     */
    public $bookService;

    /**
     * @var AuthorService
     */
    public $authorService;

    /**
     * BookController constructor.
     * @param BookService $bookService
     * @param AuthorService $authorService
     */
    public function __construct(BookService $bookService, AuthorService $authorService)
    {
        $this->bookService = $bookService;
        $this->authorService = $authorService;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $books = $this->bookService->obtainBooks();

        return $this->successResponse($books);
    }


    public function store(Request $request)
    {
        $author = $this->authorService->obtainAuthor($request->author_id);
        $book = $this->bookService->createBook($request->all(), Response::HTTP_CREATED);

        return $this->successResponse($book);
    }

    public function show($author)
    {
        $book = $this->bookService->obtainBook($author);

        return $this->successResponse($book);
    }

    public function update(Request $request, $book)
    {
        $book = $this->bookService->editBook($request->all(), $book);

        return $this->successResponse($book);
    }

    public function destroy($book)
    {
        $book = $this->bookService->deleteBook($book);

        return $this->successResponse($book);
    }

}
